extends KinematicBody2D

var speed = conf.current.GHOSTS_SPEED
var direction = null
var path2pacman = null
var ghostName = null
var change_path_cooldown = 0
var current_base = null

# Boolean qui définis le comportement de Inky
var INKY_normal_behaviour = true

# test
var BLINKY = Color(231.0/255, 76.0/255, 60.0/255)
var PINKY = Color(155.0/255, 89.0/255, 182.0/255)
var INKY = Color(46.0/255, 204.0/255, 113.0/255)
var CLYDE = Color(241.0/255, 196.0/255, 15.0/255)

var collisions = {
	'layer': [global.LAYER_GHOSTS],
	'mask': [
		global.LAYER_BULLETS,
		global.LAYER_TETRIS_SHAPE_FRIENDS,
		global.LAYER_PACMAN_KILL
	]
}

var base_list = [
	Vector2(walls.get_maze_min_size()[0], walls.get_maze_min_size()[1]),
	Vector2(walls.get_maze_max_size()[0], walls.get_maze_min_size()[1]),
	Vector2(walls.get_maze_max_size()[0], walls.get_maze_max_size()[1]),
	Vector2(walls.get_maze_min_size()[0], walls.get_maze_max_size()[1]),
]

var colors = [
	BLINKY,  # red
	CLYDE, # yellow
	INKY, # green
	PINKY, # purple
]

func _ready():
	# appear
	find_node('anim').get_animation('appear').set_length(conf.current.GHOSTS_SPAWN_DURATION)
	
	var ghosts = get_tree().get_nodes_in_group('ghosts')
	var color = false
	
	for c in colors:
		var color_is_used = false
		for g in ghosts:
			if g != self and g.get_node('sprite').self_modulate == c:
				color_is_used = true
		if not color_is_used:
			color = c
			break
	get_node('sprite').self_modulate = color
	ghostName = color
	
	# choose random position in the maze
	global.disable_collision(self)
	position = global.get_random_maze_pos()

func _physics_process(delta):
	if direction == null:
		return
	
	speed = conf.current.GHOSTS_SPEED  # will be affected by slow-mo
	
	# we have reached the first cell on path, remove it (move to next cell)
	if path2pacman.size() > 0 and global.pos_to_grid(position) == path2pacman[0]:
		path2pacman.pop_front()
		if global.DEBUG:
			global.remove_milestones(global.attach_pos_to_grid(position))
	
	# at crossroads (on grid positions), try new directions
	var gridpos = global.attach_pos_to_grid(position)
	var dist2gridpos = (position - gridpos).length()
	
	if dist2gridpos < delta*float(speed)/2: # am I on grid? (= in the middle of a cell)
		update_direction()
	
	var c = move_and_collide(direction.normalized()*speed*delta)
	if c:
		collide(c)

func path_to_pacman():
	
	# start at the ghosts (grid) position
	var gridpos = global.pos_to_grid(position)
	var paths = [ [gridpos] ]
	
	var pacman_grid_pos = find_destination()
	
	var known_cells = []
	
	var step = 0
	while true:
		step += 1
		if step >= 100:
			if self.modulate == INKY && pacman_grid_pos != global.pos_to_grid($'/root/world/pacman'.position):
				print("let me try")
				INKY_normal_behaviour = !INKY_normal_behaviour
				$timer_switch_behaviour.start()
				return path_to_pacman()
			else:
				prints('[BUG] path2pacman: too many steps, giving up',step)  # DEBUG
				find_node('anim').play('shake-and-die')
				break
			
		# try to extend current paths in all 4 directions
		var new_paths = []
		for p in paths:
			var curr_cell = p[-1]
			
			for d in range(global.DIRECTIONS.size()):
				var dir_text = global.DIRECTIONS_TEXT[d]
				var dir_ok = walls.allowed_dirs[curr_cell.x][curr_cell.y][dir_text] == 0
				
				# don't try to go through walls
				if not dir_ok:
					continue
				
				var next_cell = curr_cell + global.DIRECTIONS[d]
				# if another path already lands on next_cell, this path is useless
				if next_cell in known_cells:
					continue
				known_cells.append(next_cell)
				
				# don't add cells that are already on the path (don't go back)
				if next_cell in p:
					continue
				
				# add this next_cell to the current path
				new_paths.append(p.duplicate())
				new_paths[-1].append(next_cell)
				
				# found pacman!
				if next_cell == pacman_grid_pos:
					p.append(next_cell)
					p.pop_front()  # remove ghost starting position (reached already)
					if global.DEBUG:
						for cell in p:
							global.milestone(cell.x, cell.y, Vector2(), ghostName)
					return p
		
		# no new paths, finished exploring
#		if new_paths.size() == 0:
#			find_node('anim').play('shake-and-die')
#			break
		
		paths = new_paths
	
	return []

func find_destination():
	var pacman_grid_pos
	
	var pacman = $'/root/world/pacman'
	match ghostName:
		BLINKY:
			# comportement par défault
			
			pacman_grid_pos = global.pos_to_grid(pacman.position)
		INKY:
			# INKY possède une base dans un des 4 coins du labyrinthe
			# Il alterne entre le comportement par défault et la fuite vers sa base (10 secondes)
			
			# La base change aléatoirement et périodiquement de position parmis les 4 disponibles (22 secondes)
			# Si INKY attein sa base, il reprend le comportment par défault et reset le timer "timer_switch_behaviour"
			
			if global.DEBUG:
				global.remove_base()
				global.show_base(current_base[0], current_base[1], Vector2(), ghostName)
			
			if INKY_normal_behaviour:
				pacman_grid_pos = global.pos_to_grid(pacman.position)
			else:
				pacman_grid_pos = current_base
		CLYDE:
			# CLYDE poursuit un point définis comme étant le double du vecteur entre BLINKY et Pac-Man
			
			# Si BLINKY n'existe pas, CLYDE prend le comportement par défaut
			# Si la destination est en dehors du labyrinthe, CLYDE prend le comportement par défaut
			var destination = pacman.position
			
			var ghosts = get_tree().get_nodes_in_group('ghosts')
			for g in ghosts:
				if g != self and g.get_node('sprite').self_modulate == BLINKY:
					destination = pacman.position + pacman.position - g.position
					if !walls.is_in_maze(global.pos_to_grid(destination)):
						destination = pacman.position
			
			pacman_grid_pos = global.pos_to_grid(destination)
		PINKY:
			# Pinky a pour destination 2 cases devant pac-man (relatif a son orientation)
			
			# Si le fantôme est a moins de 3 cases de pac-man, on change son comportement.
			# On lance aussi un décompte pour ne pas qu'il revienne a son ancien comportement
			# tout de suite après son prochain déplacement.
			if change_path_cooldown > 0 || (path2pacman != null && path2pacman.size() < 3):
				if path2pacman.size() == 2:
					change_path_cooldown = 10
				pacman_grid_pos = global.pos_to_grid(pacman.position)
				change_path_cooldown -= 1
				return pacman_grid_pos
			
			pacman_grid_pos = global.pos_to_grid(pacman.position)
			var rotation_joueur = pacman.find_node('sprite').rotation_degrees
			
			# On détecte la rotation du joueur et on adapte le comportement en fonction
			match rotation_joueur:
				0.0:
					pacman_grid_pos[0] += 2
					if !walls.is_in_maze(pacman_grid_pos):
						pacman_grid_pos[0] -= 1
						if !walls.is_in_maze(pacman_grid_pos):
							pacman_grid_pos = global.pos_to_grid(pacman.position)
				90.0:
					pacman_grid_pos[1] += 2
					if !walls.is_in_maze(pacman_grid_pos):
						pacman_grid_pos[1] -= 1
						if !walls.is_in_maze(pacman_grid_pos):
							pacman_grid_pos = global.pos_to_grid(pacman.position)
				180.0:
					pacman_grid_pos[0] -= 2
					if !walls.is_in_maze(pacman_grid_pos):
						pacman_grid_pos[0] += 1
						if !walls.is_in_maze(pacman_grid_pos):
							pacman_grid_pos = global.pos_to_grid(pacman.position)
				-90.0:
					pacman_grid_pos[1] -= 2
					if !walls.is_in_maze(pacman_grid_pos):
						pacman_grid_pos[1] += 1
						if !walls.is_in_maze(pacman_grid_pos):
							pacman_grid_pos = global.pos_to_grid(pacman.position)
				_:
					# Fonction par défault si on ne peut pas détecter la rotation de pac-man
					# Prend le comportement identique au fantôme "BLINKY"
					print("[BUG] No matching rotations for Pac-Man found")
					pacman_grid_pos = global.pos_to_grid(pacman.position)
		_:
			# Fonction par défault si le fantôme n'est pas définis pas les 4 IAs proposées
			# Prend le comportement identique au fantôme "BLINKY"
			print("[BUG] No matching ghost AI")
			pacman_grid_pos = global.pos_to_grid(pacman.position)
	
	return pacman_grid_pos

func set_random_base():
	current_base = base_list[randi() % 4]

func collide(c):
	# hurt pacman and die
	if c.collider.is_in_group('pacman'):
		if c.collider.get_parent().is_shadow:  # don't hurt pacman's shadow
			c.collider.get_parent().queue_free()
			return
		
		c.collider.get_hurt()
		global.remove_from_game(self)
	
	# bounce off pacman-walls
	if c.collider.is_in_group('pacman-walls'):
		update_direction()
	
	# bounce off tris-shapes
	if c.collider.is_in_group('tris-shape'):
		# artificially remember that there is a 'wall' here (= bounce off friendly tetris shapes)
		var fake_wall_pos = global.pos_to_grid(position)
		for i in ['x','y']:
			if direction[i] > 0:
				fake_wall_pos[i] += direction[i]
		var rot = 0 if direction.x == 0 else 90
		walls.new_fake_wall(fake_wall_pos.x,fake_wall_pos.y,rot)
		
		direction  = -direction
		update_direction()

func update_direction():
	position = global.attach_pos_to_grid(position)
	var gridpos = global.pos_to_grid(position)
	
	if global.DEBUG:
		global.remove_milestones(false, ghostName)
	path2pacman = path_to_pacman()
	if path2pacman.size() == 0:
		path2pacman = [gridpos]  # already on pacman
	
	# try to reach the next cell on my path
	var next_cell = path2pacman[0]
	
	if global.DEBUG:
		global.milestone(next_cell.x,next_cell.y,Vector2(),ghostName)
	
	direction = (next_cell - gridpos).normalized()
	
	# sprite should face left or right
	if direction == global.RIGHT:
		find_node('sprite').flip_h = false
	elif direction == global.LEFT:
		find_node('sprite').flip_h = true

func _on_anim_animation_started(anim_name):
	match anim_name:
		'shake-and-die':
			global.play_sound('ghost_killed')
			modulate.a = 0.5
			global.disable_collision(self)

func _on_anim_animation_finished(anim_name):


	match anim_name:
		'appear':
			find_node('sprite').modulate.a = 1
			global.enable_collision(self)
			set_random_base()
			$timer_switch_base.start()
			$timer_switch_behaviour.start()
			update_direction()
			
		'shake-and-die':
			queue_free()


func _on_timer_switch_behaviour_timeout():
	INKY_normal_behaviour = !INKY_normal_behaviour


func _on_timer_switch_base_timeout():
	set_random_base()
